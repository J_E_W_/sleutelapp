package application.hypejelly.com.sleutelapp;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

/**
 * Created by Jelle Wielinga on 9-4-2015.
 */
public class Hoofdscherm extends Activity
{
    public static MainActivity mainActivity;
    public SharedPreferences sharedPreferences;

    public ArrayList<String> slotenlijst;
    public ArrayList<JSONObject> infoBeknoptList;
    public ArrayList<JSONObject> infoDetailList;
    public ArrayAdapter<String> adapter;

    public String ip;
    public String service;

    public TextView textview_beknopt;
    public Spinner spinner_slot;

    public void onCreate(Bundle SavedInstanceState)
    {
        super.onCreate(SavedInstanceState);
        setContentView(R.layout.hoofdscherm);
        ip = "145.101.88.185";
        getSlotenLijst();

    }

    public void getSlotenLijst()
    {
        slotenlijst = new ArrayList<String>();

        JSONObject serviceJsonObject = new JSONObject();

        try
        {
            serviceJsonObject.put("slotenlijst", "");
        } catch (JSONException error) {}

        String answer = null;

        try {
            try {
                System.out.println("console: attempt connection " + serviceJsonObject.toString());
                answer = new ServerCommunicator(mainActivity, "145.101.88.185", 4444, serviceJsonObject.toString()).execute().get();
                System.out.println("console: succesfully send json servicelijst, response = " + answer);
            } catch (ExecutionException e) {e.printStackTrace();}
        } catch (InterruptedException e1) {e1.printStackTrace();}

        String fix = answer.replace("null", "");

        JSONArray JsonArray = null;

        try
        {
            JsonArray = new JSONArray(fix);
        } catch (JSONException e) {e.printStackTrace();}

        String content = null;
        JSONObject jsonObject = null;

        for (int i = 0; i < JsonArray.length(); i++)
        {
            try
            {
                jsonObject = JsonArray.getJSONObject(i);
            } catch (JSONException e) {e.printStackTrace();}
            try
            {
                content = jsonObject.getString("naam");
            } catch (JSONException e) {e.printStackTrace();}

            slotenlijst.add(content);
        }

        System.out.println("Console: slotenlijst: " + slotenlijst);
    }
}
