package application.hypejelly.com.sleutelapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;


    public class MainActivity extends Activity
    {
        @Override
        protected void onCreate(Bundle savedInstanceState)
        {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);

            Thread welcome = new Thread() {
                public void run() {
                    try {
                        sleep(2000);
                    } catch (Exception error) {
                        error.printStackTrace();
                    } finally {
                        System.out.println("MainActivity: Hoofdscherm word gestart");
                        startActivity(new Intent(getApplicationContext(), Hoofdscherm.class));
                        System.out.println("MainActivity: Hoofdscherm gestart");
                        finish();
                    }
                }
            };
            welcome.start();
        }
    }